FROM alpine:latest
WORKDIR /usr/app
COPY ./ /usr/app
RUN apk add --update nodejs-current npm
RUN npm install -g eslint
RUN npm install -g prettier-eslint